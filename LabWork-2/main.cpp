#include <iostream>

struct Node {
    int value;
    Node *next;

    Node(int value, Node *next = nullptr) {
        this->next = next;
        this->value = value;
    }
};

class LinkedList {
private:
    Node *head;
    unsigned int current_size;

    void create(unsigned int size = 0, Node *head = nullptr) {
        this->current_size = size;
        this->head = head;
    }
public:
    LinkedList() {
        this->create();
    }

    LinkedList(LinkedList& list) {
        this->from(list);
    }

    LinkedList(unsigned int size) {
         this->create(size);
    }

    void from(LinkedList& list) {
        this->clear();
        Node *current = list.head;
        while (current != nullptr) {
            this->operator+=(current->value);
            current = current->next;
        }

    }

    ~LinkedList() {
        this->clear();
    }

    unsigned int size() {
        return this->current_size;
    }

    bool isEmpty() {
        return this->size() == 0;
    }

    void clear() {
        while (!isEmpty()) {
            removeFirst();
        }
    }

    void removeFirst() {
        Node *temp = this->head;
        this->head = head->next;
        delete temp;
        --this->current_size;
    }

    void removeAt(unsigned int index) {
        if (index == 0)
            removeFirst();
        else {
            Node *previous = this->head;
            for (unsigned int i = 0; i < index - 1; i++)
                previous = previous->next;

            Node *toDelete = previous->next;
            previous->next = previous->next->next;
            delete toDelete;
            --this->current_size;
        }
    }

    void addFirst(int value) {
        head = new Node(value, head);
        ++this->current_size;
    }

    void addAt(unsigned int index, int value) {
        if (index == 0)
            addFirst(value);
        else {
            Node* previous = this->head;

            for (unsigned int i = 0; i < index - 1; i++)
                previous = previous->next;

            Node* newNode = new Node(value, previous->next);
            previous->next = newNode;
            ++this->current_size;
        }
    }

    int operator[](unsigned int index) {
        if (index >= this->size())
            throw std::out_of_range("OutOfBounds");

        Node* current = this->head;
        for (unsigned int i = 0; i < index; i++)
            current = current->next;

        return current->value;
    }

    LinkedList& operator++() {
        this->operator+=(0);
        return *this;
    }

    LinkedList& operator+=(int b) {
        this->addAt(this->size(), b);
        return *this;
    }

    LinkedList& operator-=(unsigned int b) {
        this->removeAt(b);
        return *this;
    }

    LinkedList& operator=(LinkedList& b) {
        if (this == &b)
            return *this;

        this->from(b);
        return *this;
    }

    bool operator==(LinkedList& b) {
        if (this->size() != b.size())
            return false;

        Node *cur1 = b.head;
        Node *cur2 = this->head;

        while (cur1 != nullptr) {
            if (cur1->value != cur2->value)
                return false;

            cur1 = cur1->next;
            cur2 = cur2->next;
        }
        return true;
    }

    bool operator!=(LinkedList& b) {
        return !this->operator==(b);
    }

    bool operator<(LinkedList& b) {
        return this->size() < b.size();
    }

    bool operator>(LinkedList& b) {
        return this->size() > b.size();
    }

    bool operator<=(LinkedList& b) {
        return this->size() <= b.size();
    }

    bool operator>=(LinkedList& b) {
        return this->size() >= b.size();
    }

    friend std::ostream& operator<<(std::ostream& os, LinkedList& list);
    friend LinkedList& operator+(LinkedList& a, int b);
    friend LinkedList& operator-(LinkedList& a, unsigned int b);
};

LinkedList& operator-(LinkedList& a, unsigned int b) {
    if (b >= a.size())
        throw std::runtime_error("Out Of Bounds");

    LinkedList *list = new LinkedList();
    list->from(a);
    list->removeAt(b);
    return *list;
}

LinkedList& operator+(LinkedList& a, int b) {
    LinkedList *list = new LinkedList();
    list->from(a);
    list->addAt(list->size(), b);
    return *list;
}

std::ostream& operator<<(std::ostream& os, LinkedList& list) {
    if (list.isEmpty()) {
        os << "{ empty }";
        return os;
    }
    os << "{ ";
    Node *current = list.head;
    while (current != nullptr) {
        os << current->value << (current->next == nullptr ? "" : ", ");
        current = current->next;
    }
    os << " }";
    return os;
}

int main()
{
    LinkedList list;
    list += 400;
    list += 20;
    std::cout << "#1 = " << list << std::endl;

    LinkedList list1;
    list1 += 10;
    list1 += 20;
    list1 += 15;
    std::cout << "#2 = " << list1 << std::endl << std::endl;

    LinkedList list2;
    LinkedList list3;

    list2 = list + 25;
    std::cout << "#1 + 25 => "<< list2 << std::endl;

    list3 = list1;
    list3 += 25;
    std::cout << "#2 += 25 => " << list3 << std::endl;

    list2 = list1 - 1;
    std::cout << "#2 - 1 => "<< list2 << std::endl;

    list3 = list1;
    list3 -= 1;
    std::cout << "#2 -= 1 => " << list3 << std::endl;

    return 0;
}
