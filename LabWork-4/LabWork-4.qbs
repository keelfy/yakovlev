import qbs

CppApplication {
    Properties {
        cpp.cxxLanguageVersion: "c++11"
    }

    consoleApplication: true
    files: [
        "main.cpp",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
