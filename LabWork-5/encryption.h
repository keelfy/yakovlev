#ifndef ENCRYPTION_H
#define ENCRYPTION_H

#include <string>
#include "vector.h"

class Encryption {
protected:
    /// Вектор, содержащий в себе зашифрованный вариант строк
    Vector encrypted;
    /// Вектор, содержащий в себе расшифрованный вариант строк
    Vector decrypted;
    /// Кол-во строк в тексте
    unsigned int lines;
public:
    /// Шифрует строки в векторе decrypted и записывает результат в вектор encrypted
    virtual void encrypt_content() = 0;
    /// Расшифровывает строки из вектора encryted и записывает результат в вектор decrypted
    virtual void decrypt_content() = 0;

    /// Считывает зашифрованные строки из файла по указанному пути и сразу записывает их расшифрованную версию
    void read_encrypted_file(std::string in_path);
    /// Считывает зашированные строки из файла по указанному пути и сразу записывает их зашифрованную версию
    void read_decrypted_file(std::string in_path);

    /// Записывает в файл по указанному пути зашифрованный вариант текста
    void write_encrypted_content(std::string out_path);
    /// Записывает в файл по указанному пути расшифрованный вариант текста
    void write_decrypted_content(std::string out_path);

    /// Возвращает вектор строк зашифрованного варианта текста
    Vector get_encrypted_content();
    /// Возвращает вектор строк расшифрованного варианта текста
    Vector get_decrypted_content();

    /// Возвращает кол-во строк в тексте
    unsigned int lines_amount();

    /// Очищает все данные
    void clear();

    virtual ~Encryption();
};

#endif
