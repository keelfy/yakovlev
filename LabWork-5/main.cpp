#include <iostream>
#include <fstream>
#include "vector.h"
#include "encryption.h"

/// Класс, описывающий шфирование методом перестановки двух символов
/// P.S. Алгоритм шифровки/расшифровки идентичен
class ReplacingEncryption : public Encryption {
    /// Метод шфирует строки в векторе расшифрованных строк
    void encrypt_content() override {
        encrypted.clear();
        std::string buf, line;
        for (unsigned int i = 0; i < lines; i++)
        {
            line = decrypted.get(i);
            buf = "";

            for (unsigned int j = 0; j < line.length(); j += 2) {
                if (j + 1 == line.length()) {
                    buf += line[j];
                } else {
                    buf += line[j + 1];
                    buf += line[j];
                }
            }
            encrypted.push_back(buf);
        }
    }

    /// Расшифровывает строки в векторе зашифрованных строк
    void decrypt_content() override {
        decrypted.clear();
        std::string buf, line;
        for (unsigned int i = 0; i < lines; i++)
        {
            line = encrypted.get(i);
            buf = "";

            for (unsigned int j = 0; j < line.length(); j += 2) {
                if (j + 1 == line.length()) {
                    buf += line[j];
                } else {
                    buf += line[j + 1];
                    buf += line[j];
                }
            }
            decrypted.push_back(buf);
        }
    }
};

/// Проверка файла на существование
bool file_exists(std::string path) {
    std::ifstream file(path);
    if (!file.good()) {
        std::cout << "File " + path + " not found!" << std::endl;
        return false;
    }
    file.close();
    return true;
};

int main()
{
    ReplacingEncryption encryption;
    std::string in;
    std::string out;

    std::cout << "Enter input file name: ";
    getline(std::cin, in);

    if (!file_exists(in))
        return -1;

    std::cout << "Enter output file name: ";
    getline(std::cin, out);

    if (!file_exists(out))
        return -1;

    unsigned int action;
    std::cout << "What do you want to do?\n1. Encrypt input file\n2. Decrypt input file\n";
    std::cin >> action;

    if (action != 1 && action != 2) {
        std::cout << "Incorrect action!";
        return -1;
    }

    if (action == 1) {
        std::cout << "File encrypted!" << std::endl;
        encryption.read_decrypted_file(in);
        encryption.write_encrypted_content(out);
    } else {
        std::cout << "File decrypted!" << std::endl;
        encryption.read_encrypted_file(in);
        encryption.write_decrypted_content(out);
    }

    return 0;
}
