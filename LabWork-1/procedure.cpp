#include <iostream>
 
struct Quad {
    int x;
    int y;
    unsigned int side_length;
}; 

void set_coords(Quad quad, int x, int y, unsigned int side_length) {
	quad.x = x;
	quad.y = y;
	quad.side_length = side_length;
};
 
void set_coords(Quad quad, int x1, int y1, int x2, int y2) {
	if (x1 >= x2 || y1 <= y2 || x2 - x1 != y2 - y1)
  		throw std::runtime_error("Incorrect quad positions");
 
    quad.x = x1;
    quad.y = y1;
    quad.side_length = x2 - x1;
};

void log(Quad quad, std::ostream& os) {
	os << "X1: " << quad.x << ", Y1:" << quad.y << ", X2:" << quad.x + quad.side_length << ", Y2:" << quad.y + quad.side_length << std::endl;
};
 
unsigned int get_area(Quad quad) {
	return quad.side_length * quad.side_length;
};
 
int get_lower_right_x(Quad quad) {
	return quad.x + quad.side_length;
};
 
int get_lower_right_y(Quad quad) {
	return quad.y + quad.side_length;
};

int getPerimeter(Quad quad) {
	return quad.side_length * 4;
};

std::string render(Quad quad) {
	int i, j;
	std::string result;
	for (i = 0; i < quad.y + quad.side_length; i++) {
  		if (quad.y > i) {
    		result += '\n';
			continue;
		}
 
    	for (j = 0; j < quad.x + quad.side_length; j++) {
    		if (quad.x > j || (i != quad.y && i != quad.y + quad.side_length - 1 && j != quad.x && j != quad.x + quad.side_length - 1)) {
      			result += " ";
				continue;
			}
			result += '#';
		}
		result += '\n';
	}
	return result;
};

int main() {
    Quad quad(2, 4, 5);
  
    log(quad, std::cout);
    std::cout << "Perimeter: " << get_perimeter(quad) << ", Area:" << get_area(quad) << std::endl;
 
    render(quad);
}