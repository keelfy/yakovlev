#include <iostream>

class Quad {
    int x;
    int y;
    unsigned int side_length;
public:
    Quad() {
        this->set_coords(0, 0, 2, 2);
    }

    Quad(int x, int y, unsigned int side_length) {
        this->set_coords(x, y, side_length);
    }

    Quad(int x1, int y1, int x2, int y2) {
        this->set_coords(x1, y1, x2, y2);
    }

    void set_coords(int x, int y, unsigned int side_length) {
        this->x = x;
        this->y = y;
        this->side_length = side_length;
    }

    void set_coords(int x1, int y1, int x2, int y2) {
        if (x1 >= x2 || y1 <= y2 || x2 - x1 != y2 - y1)
            throw std::runtime_error("Incorrect quad positions");

        this->x = x1;
        this->y = y1;
        this->side_length = x2 - x1;
    }

    void log(std::ostream& os) {
        os << "X1: " << x << ", Y1:" << y << ", X2:" << x + side_length << ", Y2:" << y + side_length << std::endl;
    }

    void draw(std::ostream& os) {
        unsigned int i, j;
        for (i = 0; i < this->y + side_length; i++) {
            if (this->y > i) {
                os << std::endl;
                continue;
            }

            for (j = 0; j < this->x + side_length; j++) {
                if (this->x > j || (i != this->y && i != this->y + side_length - 1 && j != this->x && j != this->x + side_length - 1)) {
                    os << " ";
                    continue;
                }
                os << "#";
            }
            os << std::endl;
        }
    }

    unsigned int get_side_length() {
        return side_length;
    }

    unsigned int get_perimeter() {
        return side_length * 4;
    }

    unsigned int get_area() {
        return side_length * side_length;
    }

    int get_upper_left_x() {
        return x;
    }

    int get_upper_left_y() {
        return y;
    }

    int get_lower_right_x() {
        return x + side_length;
    }

    int get_lower_right_y() {
        return y + side_length;
    }
};


int main() {
    Quad quad(2, 4, 5);
    quad.log(std::cout);
    std::cout << "Perimeter: " << quad.get_perimeter() << ", Area:" << quad.get_area() << std::endl;

    quad.draw(std::cout);
}