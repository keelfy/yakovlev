import qbs

CppApplication {
    Properties {
        cpp.cxxLanguageVersion: "c++11"
    }

    consoleApplication: true
    files: [
        "arithmetic.cpp",
        "arithmetic.h",
        "geometric.cpp",
        "geometric.h",
        "main.cpp",
        "progression.cpp",
        "progression.h",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
