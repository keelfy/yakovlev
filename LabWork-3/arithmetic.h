#ifndef ARITHMETIC_H
#define ARITHMETIC_H

#include "progression.h"

class Arithmetic : public Progression
{
    double first;
    double difference;
public:
    Arithmetic(double first, double diffrence);

    double sum(int length) override;

    double get_first();

    double get_difference();

    ~Arithmetic() override;
};

#endif
