#ifndef GEOMETRIC_H
#define GEOMETRIC_H

#include "progression.h"

class Geometric : public Progression
{
    double first;
    double ratio;
public:
    Geometric(double first, double ratio);

    double sum(int length) override;

    double get_first();

    double get_ratio();

    ~Geometric() override;
};

#endif
