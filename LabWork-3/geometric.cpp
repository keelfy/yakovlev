#include "geometric.h"
#include <math.h>

Geometric::Geometric(double first, double ratio)
{
    this->first = first;
    this->ratio = ratio;
}

double Geometric::sum(int length)
{
    return first * (1 - pow(ratio, length)) / (1 - ratio);
}

double Geometric::get_first()
{
    return this->first;
}

double Geometric::get_ratio()
{
    return this->ratio;
}

Geometric::~Geometric() {}
